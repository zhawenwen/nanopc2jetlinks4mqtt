## 
本仓库用于存储本人使用mqtt与rtmp将nanopc-T4开发板采集到的传感器数据、视频数据、音频数据实时传输到 jetlinks 物联网平台中，同时开发板能实时接受物联网平台下发的控制信息

## 1. 目录 `test_004_basicFountion` 实现以下功能

本目录文件为 nonapc-t4 开发板与 jetlinks 物联网平台联合调试代码

 **本代码实现以下功能：**
 1. 通过 mqtt 协议实时推送本地传感器数据（氨气+温度+湿度+CO2+光照轻度）到 jetlinks 平台
 2. 通过 rtmp 协议实时推送采集到的摄像头视频数据到 jetlinks 平台展示，浏览器需要支持flash
 3. 通过 mqtt 协议实时接受 jetlinks 平台下发的控制信息并实时驱动舵机转动

 **本代码暂未实现功能：**
 1. 摄像头采集到的视频数据暂未加入识别算法
 2. jetlinks 接受并展示的rtmp 视频数据暂时无法保存
 3. 音频数据暂未实现采集

> 相关注意事项：
> - jetlinks 平台链接：http://iot2.gitnote.com  通过 natfrp 内网穿透服务暴露于外> 网中，内网地址为：172.17.14.191:9000
> 
> - rtmp 服务器（tiangolo/nginx-rtmp）通过 docker部署与 1.15 开头的服务器中，通> 过 http://iot.gitnote.cn 或 1.15 ip 地址可访问到 ，默认端口号为：1935
> 
> - mqtt 服务器通过docker 部署在内网工作站中，内网ip为：172.17.14.191:8848，通过 https://lanp.nioee.com/ 内网穿透服务暴露与外网中，外网地址为：139.155.180.XXX:10070

## 2. 目录 `test_005_audio` 实现以下功能

文件 rtmp_bash.txt 实现实时采集音频视频并推送到rtmp服务器中

文件 rtmp_001.py 实现opencv采集图像与音频合并推送到rtmp

详见 [test_005_audio/readme.md](https://gitee.com/qiaoyukeji/nanopc2jetlinks4mqtt/blob/master/test_005_audio/readme.md)
