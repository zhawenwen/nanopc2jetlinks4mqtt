# 需先自行安装FFmpeg，并添加环境变量
import cv2
import subprocess
import datetime
import numpy as np
from PIL import Image, ImageDraw, ImageFont

# RTMP服务器地址
# rtmp = r'rtmp://156756.livepush.myqcloud.com/live/test'
# rtmp = r'rtmp://172.17.14.191/live/test'
rtmp = r'rtmp://42.192.123.52/live/test'
# 读取视频并获取属性
cap = cv2.VideoCapture(10)

cap.set(3, 640) # set video widht
cap.set(4, 480) # set video height
size = (int(640), int(480))
sizeStr = str(size[0]) + 'x' + str(size[1])
command = ['ffmpeg',
    '-y', '-an',
    '-f', 'rawvideo',
    '-vcodec','rawvideo',
    '-pix_fmt', 'bgr24',
    '-s', sizeStr,
    '-r', '25',
    '-i', '-',
    '-c:v', 'libx264',
    '-pix_fmt', 'yuv420p',
    '-preset', 'ultrafast',
    '-f', 'flv',
    rtmp]
pipe = subprocess.Popen(command, shell=False, stdin=subprocess.PIPE
)
font = cv2.FONT_ITALIC   # 字体
fourcc = cv2.VideoWriter_fourcc(*'XVID')  #定义编解码器并创建VideoWriter对象，即输入四个字符代码即可得到对应的视频编码器（XVID编码器）
# out = cv2.VideoWriter('video1.avi', fourcc, 20.0, (640,480))  #参数：保存文件名，编码器，帧率，视频宽高



def cv2ImgAddText(img, text, left, top, textColor=(0, 255, 0), textSize=20):
    if (isinstance(img, np.ndarray)):  # 判断是否OpenCV图片类型
        img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # 创建一个可以在给定图像上绘图的对象
    draw = ImageDraw.Draw(img)
    # 字体的格式
    fontStyle = ImageFont.truetype(
        "simsun.ttc", textSize, encoding="utf-8")
    # 绘制文本
    draw.text((left, top), text, textColor, font=fontStyle)
    # 转换回OpenCV格式
    return cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR)


while cap.isOpened():
    success, frame = cap.read()
    cv2.putText(frame,(datetime.datetime.utcnow() + datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S'), (15,40),font , 1, (255,255,255), 2) 
    # cv2.putText(frame,'安徽农业大学智慧养殖平台：HF-001 ', (15,400),font , 1, (255,255,255), 2) 
    # frame=cv2ImgAddText(frame,"安徽农业大学智慧养殖平台：HF-XX-001",15,50,(255,255,255),24)
    if success:
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        pipe.stdin.write(frame.tostring())
        # out.write(frame)

cap.release()
pipe.terminate()
cv2.destroyAllWindows()