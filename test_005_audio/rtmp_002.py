# 本代码实现 opencv 采集视频并使用ffmpeg与音频audio合并推送到远程 rtmp 服务器中 

# 需先自行安装FFmpeg，并添加环境变量
import cv2
import subprocess
import datetime
import numpy as np
from PIL import Image, ImageDraw, ImageFont

# RTMP服务器地址
# rtmp = r'rtmp://156756.livepush.myqcloud.com/live/test'
# rtmp = r'rtmp://172.17.14.191/live/test'
rtmp = r'rtmp://42.192.123.52/live/test'
# 读取视频并获取属性
cap = cv2.VideoCapture(10)

cap.set(3, 640) # set video widht
cap.set(4, 480) # set video height
size = (int(640), int(480))
sizeStr = str(size[0]) + 'x' + str(size[1])
command = [

    'ffmpeg',
    '-thread_queue_size','4096',
    '-f', 'alsa', # -f fmt	指定格式(音频或视频格式)
    '-ac', '1', # -ac channels	设置声道数
    '-i', 'hw:4,0', # -i filename	指定输入文件名，在linux下当然也能指定:0.0(屏幕录制)或摄像头
    '-r','48000',
    '-thread_queue_size','4096',
    '-f', 'rawvideo',  # -f fmt	指定格式(音频或视频格式)
    '-vcodec','rawvideo', # -vcodec codec	强制使用codec编解码方式
    '-pix_fmt', 'bgr24', # -pix_fmt format  设置像素格式，“列表”作为参数显示
    '-s', sizeStr, # -s size	分辨率
    # '-f','v412',
    '-r','10',
    '-i', '-',
    '-c:v', 'libx264',
    '-s', sizeStr, # -s size	分辨率           
    '-pix_fmt', 'yuv420p',
    '-preset', 'ultrafast',
        # '-b:v','1000k',


     ########### 音频设置相关参数
    '-c:a', 'aac', # 设置音频格式
    '-ar', '44100',  # -ar rate	设置音频采样率 (单位：Hz)

    '-f', 'flv',
    rtmp]
pipe = subprocess.Popen(command, shell=False, stdin=subprocess.PIPE)

font = cv2.FONT_ITALIC   # 字体
fourcc = cv2.VideoWriter_fourcc(*'XVID')  #定义编解码器并创建VideoWriter对象，即输入四个字符代码即可得到对应的视频编码器（XVID编码器）
out = cv2.VideoWriter('video1.avi', fourcc, 20.0, (640,480))  #参数：保存文件名，编码器，帧率，视频宽高



def cv2ImgAddText(img, text, left, top, textColor=(0, 255, 0), textSize=20):
    if (isinstance(img, np.ndarray)):  # 判断是否OpenCV图片类型
        img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # 创建一个可以在给定图像上绘图的对象
    draw = ImageDraw.Draw(img)
    # 字体的格式
    fontStyle = ImageFont.truetype(
        "simsun.ttc", textSize, encoding="utf-8")
    # 绘制文本
    draw.text((left, top), text, textColor, font=fontStyle)
    # 转换回OpenCV格式
    return cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR)


while cap.isOpened():
    success, frame = cap.read()
    cv2.putText(frame,(datetime.datetime.utcnow() + datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S'), (15,40),font , 1, (255,255,255), 2) 
    # cv2.putText(frame,'安徽农业大学智慧养殖平台：HF-001 ', (15,400),font , 1, (255,255,255), 2) 
    # frame=cv2ImgAddText(frame,"安徽农业大学智慧养殖平台：HF-XX-001",15,50,(255,255,255),24)
    if success:
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        pipe.stdin.write(frame.tostring())
        out.write(frame)

cap.release()
pipe.terminate()
cv2.destroyAllWindows()