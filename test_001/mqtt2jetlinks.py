# -*- coding: utf-8 -*-

from paho.mqtt import client as mqtt

import hashlib
MQTTHOST = "172.17.14.191"
MQTTPORT = 1883
mqttClient = mqtt.Client()
topic_1 = "/ZN_001/ZN_001_001/properties/report"
topic_2 = "/ZN_001/ZN_001_001/function/invoke"
# generate client ID with pub prefix randomly
client_id = "ZN_001_001"
import time
 
t = time.time()

def str2md5(str):
    '''使用MD5对字符串进行加密

    Args:
        str (str): 需要加密的字符串

    Returns:
        [str]: 32位字符串
    ''' 
    m = hashlib.md5()  # 创建md5对象
    
    str_en = str.encode(encoding='utf-8')  # str必须先encode
    m.update(str_en)  # 传入字符串并加密
    str_md5 = m.hexdigest()  # 将MD5 hash值转换为16进制数字字符串
    return str_md5

 
# 13位 时间戳 (毫秒)
int(round(t * 1000))

secureId= 'ahau'
secureKey='ahau'
now = int(round(t * 1000)); #//当前时间戳
username_1 = secureId+"|"+str(now); #// 拼接用户密码
password_1 = str2md5(username_1+"|"+secureKey); #//使用md5生成摘要

print("username_1="+username_1)
print("password_1="+password_1)
# 连接MQTT服务器
def on_mqtt_connect():
    mqttClient.username_pw_set(username_1,password_1)
    mqttClient.connect(MQTTHOST, MQTTPORT, 60)
    mqttClient.loop_start()


# publish 消息
def on_publish(topic, payload, qos):
    print("payload=",payload)
    mqttClient.publish(topic, payload, qos)

# 消息处理函数
def on_message_come(lient, userdata, msg):

    print(msg.topic + " " + ":" + str(msg.payload))


# subscribe 消息
def on_subscribe():
    mqttClient.subscribe(topic_2, 1)
    mqttClient.on_message = on_message_come # 消息到来处理函数

msg=str({"properties": {"wendu": 34,"shidu":50,"guangzhao":390,"co2":200,"NH3":3},"deviceId": "ZN_001_001","success": 'true'})
def main():
    on_mqtt_connect()
    while True:
        on_mqtt_connect()
        time.sleep(3)
        on_publish(topic_1, msg, 1)
        print(111)
        on_subscribe()
        print(222)



if __name__ == '__main__':
    main()

