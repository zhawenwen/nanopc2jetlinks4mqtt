# -*- coding: utf-8 -*-
# @Time : 2020/3/16 0016 9:32
# @Author : Liqiju
# @File : MQTTSubscribe.py
# @Software : PyCharm
import paho.mqtt.client as mqtt
import time
import hashlib


t = time.time()

def str2md5(str):
    '''使用MD5对字符串进行加密

    Args:
        str (str): 需要加密的字符串

    Returns:
        [str]: 32位字符串
    ''' 
    m = hashlib.md5()  # 创建md5对象
    
    str_en = str.encode(encoding='utf-8')  # str必须先encode
    m.update(str_en)  # 传入字符串并加密
    str_md5 = m.hexdigest()  # 将MD5 hash值转换为16进制数字字符串
    return str_md5

 
# 13位 时间戳 (毫秒)
int(round(t * 1000))

secureId= 'ahau'
secureKey='ahau'
now = int(round(t * 1000)); #//当前时间戳
username_1 = secureId+"|"+str(now); #// 拼接用户密码
password_1 = str2md5(username_1+"|"+secureKey); #//使用md5生成摘要

print("username_1="+username_1)
print("password_1="+password_1)

#MQTT服务器
host = "172.17.14.191"
#端口
port = 1883
#主题
topic = "/ZN_001/ZN_001_001/function/invoke"
 
def on_connect(client, userdata, flags, rc):
  print("Connected with result code "+str(rc))
  client.subscribe(topic)
 
def on_message(client, userdata, msg):
  print(msg.topic+" " + ":" + str(msg.payload))
 
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(username_1,password_1)

client.connect(host, port, 60)
# client.loop_forever() #保持连接，有消息就接受打印出来