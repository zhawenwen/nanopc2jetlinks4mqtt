import cv2


cam = cv2.VideoCapture(0)
cam.set(3, 640) # set video widht
cam.set(4, 480) # set video height
while True:
    ret, img =cam.read()  # 读取一帧图像         
      
    cv2.imshow('camera',img) 
    k = cv2.waitKey(10) & 0xff # Press 'ESC' for exiting video
    if k == 27:
        break
 

cam.release()

cv2.destroyAllWindows()


